# ✅ LaTeX Reference Order Checker

This is a tool to check that the `\label`s are `\(auto)ref`erenced in the correct order.

## Installation

If you use [the nix package manager or NixOS](https://nixos.org/), then you can use this as a flake right away:

```bash
nix run gitlab:nobodyinperson/latex-check-refs -- myfile.tex
```

Othewise, download [the script](https://gitlab.com/nobodyinperson/latex-check-refs/-/raw/main/latex-check-refs):
    
```bash
# with wget
wget https://gitlab.com/nobodyinperson/latex-check-refs/-/raw/main/latex-check-refs
# or with curl
curl -O https://gitlab.com/nobodyinperson/latex-check-refs/-/raw/main/latex-check-refs
```

## Usage

```bash
python latex-check-refs myfile.tex
# only consider labels starting with SECTION:
python latex-check-refs --only '^SECTION' myfile.tex 
# ignore containing TABLE
python latex-check-refs --ignore 'TABLE' myfile.tex 
```

```bash
python latex-check-refs --help
usage: latex-check-refs [-h] [-v] [-k] [--ignore IGNORE [IGNORE ...]]
                        [--only ONLY [ONLY ...]]
                        [input]

Check a LaTeX file for correct order of figure and table referencing

positional arguments:
  input

options:
  -h, --help            show this help message and exit
  -v, --verbose
  -k, --keep-going      don't stop after the first error
  --ignore IGNORE [IGNORE ...]
                        Regular expressions to filter out labels (e.g.
                        ^SECTION to ignore labels starting with SECTION)
  --only ONLY [ONLY ...]
                        Regular expressions to consider only certain labels
                        (e.g. ^TABLE: to process only labels starting with
                        TABLE:)
```

## Example

```tex
python latex-check-refs <<EOT
\label{label1}
\label{label2}
\label{label3}
\ref{label1}
\ref{label2}
\ref{label2}
EOT
# 💥 Label label3 is never referenced
#
#
# Fix your REFERENCING order:
#
#   label1
#   label2
# + label3
#
#
# Alternatively fix your PLACEMENT order:
#
#   label1
#   label2
# - label3
```
