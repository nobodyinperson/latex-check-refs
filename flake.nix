{
  description = " LaTeX Reference Order Checker";
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };
  outputs = { self, flake-utils, nixpkgs, ... }:
    flake-utils.lib.eachSystem flake-utils.lib.allSystems (system:
      let
        pkgs = nixpkgs.legacyPackages."${system}";
        launcherName = "latex-check-refs";
        launcher = pkgs.writeScriptBin launcherName
          ''${pkgs.python3}/bin/python ${./latex-check-refs} "$@"'';
      in {
        apps.default = {
          type = "app";
          program = "${launcher}/bin/${launcherName}";
        };
        devShells.default =
          pkgs.mkShell { buildInputs = with pkgs; [ launcher ]; };
      });
}
